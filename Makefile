###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2012  IPFire Development Team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

LDIFS     = $(sort $(wildcard templates/*.ldif))
TEMPLATE  = template.ldif

PREFIX  = /usr
DATADIR = $(PREFIX)/share/ipfire-dit

.PHONY: all
all: $(TEMPLATE)

$(TEMPLATE): $(LDIFS)
	for ldif in $^; do \
		echo -e "# Source: $${ldif}"; \
		cat $${ldif}; echo ""; \
	done > $@

.PHONY: install
install: $(TEMPLATE)
	# Install all of the templates.
	-mkdir -pv $(DATADIR)
	cp -vf $(TEMPLATE) $(DATADIR)

.PHONY: clean
clean:
	rm -f $(TEMPLATE)
